package device

import (
	"context"
	"fmt"
	"gitee.com/czy_hit/wireguard-go/tun"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"golang.org/x/net/ipv4"
	"net"
	"os/exec"
	"testing"
	"time"
)

const (
	DefaultDeviceName = "tun1"
	DefaultBatchSize  = 16
)

func TestBasicTUNInterface(t *testing.T) {
	tunDevice, err := tun.CreateTUN(DefaultDeviceName, DefaultMTU)
	if err != nil {
		t.Fatal("failed to create tun device")
	}
	t.Logf("create tun device success\n")
	defer tunDevice.Close()

	bufs := make([][]byte, DefaultBatchSize)
	for i := range bufs {
		bufs[i] = make([]byte, MaxMessageSize)
	}
	sizes := make([]int, DefaultBatchSize)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				return

			default:
			}

			count, readErr := tunDevice.Read(bufs, sizes, MessageTransportOffsetContent)
			{
				if readErr != nil {
					t.Error(readErr)
				}
				for i := 0; i < count; i++ {
					fmt.Printf("tun packet size:%d \n", sizes[i])
					fmt.Println(bufs[i][MessageTransportOffsetContent : MessageTransportOffsetContent+sizes[i]])
					//icmpv4(t,bufs[i]);
					udp(t, bufs[i][MessageTransportOffsetContent:MessageTransportOffsetContent+sizes[i]])
				}

			}

		}

	}(ctx)

	setupIfce(t, net.IPNet{
		IP:   net.IPv4(192, 168, 100, 1),
		Mask: net.IPv4Mask(255, 255, 255, 0),
	}, DefaultDeviceName)
	defer downIfce(t, DefaultDeviceName)

	//startPing(t, net.IPv4(192, 168, 100, 1), false)
	time.Sleep(time.Second * 60)

}

func setupIfce(t *testing.T, ipNet net.IPNet, dev string) {
	if err := exec.Command("ip", "link", "set", dev, "up").Run(); err != nil {
		t.Fatal(err)
	}
	if err := exec.Command("ip", "addr", "add", ipNet.String(), "dev", dev).Run(); err != nil {
		t.Fatal(err)
	}
}

func downIfce(t *testing.T, ifceName string) {
	if err := exec.Command("ip", "link", "set", ifceName, "down").Run(); err != nil {
		t.Fatal(err)
	}
}

//func startPing(t *testing.T, dst net.IP, dashB bool) {
//	params := []string{"-c", "4", dst.String()}
//	if dashB {
//		params = append([]string{"-b"}, params...)
//	}
//	if err := exec.Command("ping", params...).Start(); err != nil {
//		t.Fatal(err)
//	}
//}

//func icmpv4(t *testing.T, buf []byte) {
//	packet := gopacket.NewPacket(buf, layers.LayerTypeICMPv4, gopacket.Default)
//	if icmpLayer := packet.Layer(layers.LayerTypeICMPv4); icmpLayer != nil {
//		icmp, _ := icmpLayer.(*layers.ICMPv4)
//		fmt.Println("This is a ICMP packet!")
//		fmt.Printf("icmp.Id %d,icmp.Seq %d\n", icmp.Id, icmp.Seq)
//	}
//}

func udp(t *testing.T, buf []byte) {
	packet := gopacket.NewPacket(buf, layers.LayerTypeIPv4, gopacket.Default)
	header, _ := ipv4.ParseHeader(buf)
	fmt.Printf("src ip:%s,dst ip:%s\n", header.Src, header.Dst)
	if udpLayer := packet.Layer(layers.LayerTypeUDP); udpLayer != nil {
		udp, _ := udpLayer.(*layers.UDP)
		fmt.Printf("udp SrcPort %d, DstPort %d,payload %s\n", udp.SrcPort, udp.DstPort, string(udp.Payload))
		conn, err := net.Dial("udp", fmt.Sprintf("%s:%d", header.Src.String(), udp.SrcPort))
		if err != nil {
			t.Errorf("failed connected to target device：%v\n", err)
			return
		}
		defer conn.Close()
		message := []byte(fmt.Sprintf("Server has already received your message:%s\n", string(udp.Payload)))
		_, err = conn.Write(message)
		if err != nil {
			t.Errorf("failed to send data：%v\n", err)
			return
		}
	}
}
