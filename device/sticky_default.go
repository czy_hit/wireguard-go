//go:build !linux

package device

import (
	"gitee.com/czy_hit/wireguard-go/conn"
	"gitee.com/czy_hit/wireguard-go/rwcancel"
)

func (device *Device) startRouteListener(bind conn.Bind) (*rwcancel.RWCancel, error) {
	return nil, nil
}
